﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 文本是否存在
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string file = "";
        List<string> list = new List<string>();
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件夹";
            dialog.Filter = "所有文件(*.*)|*.*";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                file = dialog.FileName;
                label1.Text = file;
                Read(file);
            }
        }
        public void Read(string path)
        {
            list.Clear();
            StreamReader sr = new StreamReader(path, Encoding.Default);
            String line;
            while ((line = sr.ReadLine()) != null)
            {
                //  Console.WriteLine(line.ToString());
                list.Add(line.ToString());
            }
            label2.Text = "共"+ list.Count + "行";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            var key = textBox1.Text.Trim();
            var n = 100;
            foreach (var item in list)
            {
                if (item.Contains(key))
                {
                    listBox1.Items.Add(item.ToString());
                    if (listBox1.Items.Count>=n)
                    {
                        label2.Text = "仅显示前";
                        break;
                    }
                    else
                    {
                        label2.Text = "";
                    }

                }

            }
            label2.Text += listBox1.Items.Count.ToString()+"项";
        }
    }
}
